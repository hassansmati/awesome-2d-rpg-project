using UnityEngine;
using TMPro;

/// <summary>
/// Manages the game's currency system and updates the UI accordingly.
/// </summary>
public class GameManager : MonoBehaviour
{
    /// <summary>
    /// Singleton instance.
    /// </summary>
    public static GameManager Instance;

    /// <summary>
    /// Initial amount of money the player starts with.
    /// </summary>
    public int startingMoney = 100;

    /// <summary>
    /// Current amount of money the player has.
    /// </summary>
    public int money;

    /// <summary>
    /// Reference to the UI element displaying player's money in the inventory.
    /// </summary>
    public GameObject inventoryplayerCoinsUI;

    /// <summary>
    /// Reference to the UI element displaying player's money globally.
    /// </summary>
    public GameObject globalplayerCoins;

    /// <summary>
    /// Property to get or set the player's money.
    /// </summary>
    public int Money
    {
        get { return money; }
        set { money = value; }
    }

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// It initializes the player's money and updates the UI.
    /// </summary>
    void Awake()
    {
        money = startingMoney;
        UpdateCoinsUI();
    }

    /// <summary>
    /// Updates the UI elements displaying the player's money.
    /// </summary>
    public void UpdateCoinsUI()
    {
        inventoryplayerCoinsUI.GetComponent<TextMeshProUGUI>().text = money.ToString();
        globalplayerCoins.GetComponent<TextMeshProUGUI>().text = money.ToString();
    }

    /// <summary>
    /// Start is called before the first frame update.
    /// It initializes the player's money.
    /// </summary>
    void Start()
    {
        money = startingMoney;
    }
}
