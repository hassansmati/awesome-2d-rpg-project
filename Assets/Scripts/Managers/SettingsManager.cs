using UnityEngine;
using UnityEngine.Audio;

/// <summary>
/// Manages game settings such as sound volume and fullscreen/windowed mode.
/// </summary>
public class SettingsManager : MonoBehaviour
{
    /// <summary>
    /// Reference to the audio mixer for controlling sound volume.
    /// </summary>
    public AudioMixer audioMixer;

    /// <summary>
    /// Loads settings when the game starts.
    /// </summary>
    private void Start()
    {
        LoadSettings();
    }

    /// <summary>
    /// Loads settings from PlayerPrefs.
    /// </summary>
    private void LoadSettings()
    {
        // Load sound volume
        float soundVolume = PlayerPrefs.GetFloat("SoundVolume", 1f); // Default volume: 100%
        SetSoundVolume(soundVolume);

        // Load fullscreen/windowed mode
        bool isFullscreen = PlayerPrefs.GetInt("IsFullscreen", 1) == 1; // Default: Fullscreen
        SetFullscreen(isFullscreen);
    }

    /// <summary>
    /// Saves settings to PlayerPrefs.
    /// </summary>
    public void SaveSettings()
    {
        // Save sound volume
        PlayerPrefs.SetFloat("SoundVolume", AudioListener.volume);

        // Save fullscreen/windowed mode
        int fullscreenValue = Screen.fullScreen ? 1 : 0;
        PlayerPrefs.SetInt("IsFullscreen", fullscreenValue);

        PlayerPrefs.Save();
    }

    /// <summary>
    /// Sets sound volume using the audio mixer.
    /// </summary>
    /// <param name="volume">The volume level to set.</param>
    public void SetSoundVolume(float volume)
    {
        audioMixer.SetFloat("volume", volume);
    }

    /// <summary>
    /// Sets fullscreen/windowed mode.
    /// </summary>
    /// <param name="isFullscreen">True to set fullscreen, false for windowed mode.</param>
    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }

    /// <summary>
    /// Toggles between fullscreen and windowed mode.
    /// </summary>
    public void ToggleFullscreen()
    {
        SetFullscreen(!Screen.fullScreen);
    }

    /// <summary>
    /// Quits the game.
    /// </summary>
    public void QuitGame()
    {
        SaveSettings(); // Save settings before quitting
        Application.Quit();
    }
}
