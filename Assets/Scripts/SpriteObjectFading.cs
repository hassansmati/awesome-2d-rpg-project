using System.Collections;
using System.Diagnostics;
using UnityEngine;

/// <summary>
/// Provides functionality to fade a sprite renderer when a player enters or exits its trigger zone.
/// </summary>
public class SpriteObjectFading : MonoBehaviour
{
    /// <summary>
    /// Reference to the sprite renderer component.
    /// </summary>
    public SpriteRenderer houseSpriteRenderer;

    /// <summary>
    /// Duration of the fade effect.
    /// </summary>
    public float fadeDuration = 1f;

    /// <summary>
    /// Store the original color of the sprite.
    /// </summary>
    private Color originalColor;

    /// <summary>
    /// Flag to prevent multiple fading coroutines.
    /// </summary>
    private bool isFading = false;

    /// <summary>
    /// Rate at which the sprite fades.
    /// </summary>
    public float fadeRate = 0.2f;

    /// <summary>
    /// Start is called before the first frame update.
    /// It initializes the original color of the sprite.
    /// </summary>
    private void Start()
    {
        if (houseSpriteRenderer == null)
        {
            houseSpriteRenderer = GetComponent<SpriteRenderer>();
        }
        originalColor = houseSpriteRenderer.color;
    }

    /// <summary>
    /// OnTriggerEnter2D is called when the Collider2D other enters the trigger.
    /// It starts the fade in effect when the player enters the trigger zone.
    /// </summary>
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            StartFade(originalColor, new Color(originalColor.r, originalColor.g, originalColor.b, fadeRate));
        }
    }

    /// <summary>
    /// OnTriggerExit2D is called when the Collider2D other has stopped touching the trigger.
    /// It starts the fade out effect when the player exits the trigger zone.
    /// </summary>
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            StartFade(new Color(originalColor.r, originalColor.g, originalColor.b, fadeRate), originalColor);
        }
    }

    /// <summary>
    /// Initiates the fade effect by starting the fade coroutine.
    /// </summary>
    private void StartFade(Color startColor, Color targetColor)
    {
        StopCoroutine(FadeSprite(startColor, targetColor));
        StartCoroutine(FadeSprite(startColor, targetColor));
    }

    /// <summary>
    /// Coroutine to perform the fade effect gradually.
    /// </summary>
    private IEnumerator FadeSprite(Color startColor, Color targetColor)
    {
        isFading = true;
        float elapsedTime = 0f; // Time elapsed during the fade effect

        // Gradually fade the alpha value
        while (elapsedTime < fadeDuration)
        {
            float t = Mathf.SmoothStep(0f, 1f, elapsedTime / fadeDuration); // SmoothStep for smoother interpolation
            houseSpriteRenderer.color = Color.Lerp(startColor, targetColor, t);
            elapsedTime += Time.deltaTime;
            yield return null; // Wait for the next frame
        }

        // Ensure the color reaches the target color precisely
        houseSpriteRenderer.color = targetColor;
        isFading = false;
    }
}
