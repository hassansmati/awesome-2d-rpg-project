using UnityEngine;

/// <summary>
/// Handles player movement using Rigidbody2D.
/// </summary>
public class PlayerMovement : MonoBehaviour
{
    /// <summary>
    /// Adjust this to change the speed of movement.
    /// </summary>
    public float moveSpeed = 5f;

    private Rigidbody2D rb;
    private bool isMoving = false;
    private bool flipX = false;
    public bool canMove = false; // Determines if player movement is enabled

    /// <summary>
    /// Reference to the Animator component.
    /// </summary>
    public Animator animator;

    /// <summary>
    /// Start is called before the first frame update.
    /// It gets the Rigidbody2D component.
    /// </summary>
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    /// <summary>
    /// FixedUpdate is called a fixed number of times per second.
    /// It handles player movement based on user input.
    /// </summary>
    void FixedUpdate()
    {
        if (canMove)
        {
            // Get input from arrow keys
            float horizontalInput = Input.GetAxis("Horizontal");
            float verticalInput = Input.GetAxis("Vertical");

            // Calculate movement direction
            Vector2 movement = new Vector2(horizontalInput, verticalInput).normalized;

            // Check if movement input is detected
            if (movement != Vector2.zero)
            {
                isMoving = true;
                UpdateAnimator(isMoving);

                // Set flipX based on horizontal movement direction
                flipX = horizontalInput < 0;
            }
            else
            {
                isMoving = false;
                UpdateAnimator(isMoving);
            }

            // Move the player using Rigidbody2D
            rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);

            // Flip the character sprite if necessary
            FlipCharacter();
        }
    }

    /// <summary>
    /// Updates the animator based on player movement.
    /// </summary>
    void UpdateAnimator(bool run)
    {
        if (animator != null)
        {
            animator.SetBool("Run", run);
        }
    }

    /// <summary>
    /// Flips the character sprite horizontally if necessary.
    /// </summary>
    void FlipCharacter()
    {
        if (flipX)
        {
            transform.localScale = new Vector3(-1f, 1f, 1f); // Flip character horizontally
        }
        else
        {
            transform.localScale = new Vector3(1f, 1f, 1f); // Reset character scale
        }
    }

    /// <summary>
    /// Disables player movement.
    /// </summary>
    public void DisablePlayerController()
    {
        canMove = false;
    }

    /// <summary>
    /// Enables player movement.
    /// </summary>
    public void EnablePlayerController()
    {
        canMove = true;
    }
}
