using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// Represents a clothing item in the game.
/// Allows the player to interact with the item, adding it to the inventory when clicked.
/// </summary>
public class ClothingItem : MonoBehaviour
{
    /// <summary>
    /// Reference to the clothing item data.
    /// </summary>
    public Item item;

    /// <summary>
    /// Reference to the inventory.
    /// </summary>
    public Inventory inventory;

    /// <summary>
    /// Reference to the inventory UI for updating visuals.
    /// </summary>
    public InventoryUI inventoryUI;

    /// <summary>
    /// Reference to the text component displaying the price.
    /// </summary>
    public TextMeshProUGUI priceText;

    /// <summary>
    /// Reference to the image component displaying the icon.
    /// </summary>
    public Image iconImage;

    /// <summary>
    /// Initializes the clothing item with the provided data.
    /// Sets the item, inventory, and inventoryUI references.
    /// Initializes the price text component and updates the price display.
    /// </summary>
    public void Initialize(Item item, Inventory inventory, InventoryUI inventoryUI)
    {
        this.item = item;
        this.inventory = inventory;
        this.inventoryUI = inventoryUI;

        // Find and initialize the price text component.
        priceText = transform.Find("Item_Price")?.GetComponent<TextMeshProUGUI>();
        if (priceText == null)
        {
            Debug.LogError("Price text component not found.");
        }
        else
        {
            UpdatePriceText();
        }

        // Find and initialize the icon image component.
        iconImage = transform.Find("Item_Image")?.GetComponent<Image>();
        if (iconImage == null)
        {
            Debug.LogError("Icon image component not found.");
        }
        else
        {
            UpdateImageIcon();
        }
    }

    /// <summary>
    /// Updates the price text display with the item's price.
    /// </summary>
    private void UpdatePriceText()
    {
        if (item != null && priceText != null)
        {
            priceText.text = item.price.ToString();
        }
    }

    /// <summary>
    /// Updates the icon image display with the item's icon.
    /// </summary>
    private void UpdateImageIcon()
    {
        if (item != null && iconImage != null && item.icon != null)
        {
            iconImage.sprite = item.icon;
        }
    }

    /// <summary>
    /// Called when the object becomes enabled and active.
    /// Initializes references such as the button component.
    /// Updates the price text display.
    /// </summary>
    private void Start()
    {
        InitializeReferences();
       
    }

    /// <summary>
    /// Initializes necessary references like the button component.
    /// Updates the price text display.
    /// </summary>
    private void InitializeReferences()
    {
        Button button = GetComponent<Button>();
        if (button != null)
        {
            button.onClick.AddListener(AddToInventory);
        }
        else
        {
            Debug.LogError("Button component not found.");
        }
        // Find and initialize the price text component.
        if (transform.Find("Item_Price"))
        {
            priceText = transform.Find("Item_Price")?.GetComponent<TextMeshProUGUI>();
            UpdatePriceText();
        }
        // Find and initialize the icon image component.
        if (transform.Find("Item_Image"))
        {
            iconImage = transform.Find("Item_Image")?.GetComponent<Image>();
            UpdateImageIcon();

        }
        
        UpdateImageIcon();
        UpdatePriceText();
    }

    /// <summary>
    /// Adds the clothing item to the inventory when clicked.
    /// Calls the inventory's BuyItem method with the item's price.
    /// Updates the inventory UI.
    /// </summary>
    private void AddToInventory()
    {
        if (inventory != null && item != null)
        {
            inventory.BuyItem(item, item.price);
            inventoryUI?.UpdateInventoryUI();
        }
        else
        {
            Debug.LogWarning("Inventory or item is not assigned.");
        }

       
    }
}
