using Unity.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using static Item;

/// <summary>
/// Manages the equipment of a player object, such as equipping and unequipping items.
/// </summary>
public class EquipmentManager : MonoBehaviour
{
    /// <summary>
    /// Reference to the object that will be equipped with items.
    /// </summary>
    public GameObject playerObject;

    /// <summary>
    /// Reference to the object representing the head slot.
    /// </summary>
    public GameObject headObject;

    /// <summary>
    /// Reference to the object representing the torso slot.
    /// </summary>
    public GameObject torse;

    /// <summary>
    /// Reference to the object representing the pelvis slot.
    /// </summary>
    public GameObject pelvis;

    /// <summary>
    /// Reference to the object representing the left leg slot.
    /// </summary>
    public GameObject lLeageadObject;

    /// <summary>
    /// Reference to the object representing the right leg slot.
    /// </summary>
    public GameObject RLeageadObject;

    /// <summary>
    /// Reference to the object representing the left shoe slot.
    /// </summary>
    public GameObject LShoesObject;

    /// <summary>
    /// Reference to the object representing the right shoe slot.
    /// </summary>
    public GameObject RShoesObject;

    /// <summary>
    /// The item currently equipped in the head slot.
    /// </summary>
    private Item equippedHeadItem;

    /// <summary>
    /// The item currently equipped in the body slot.
    /// </summary>
    private Item equippedBodyItem;

    /// <summary>
    /// The item currently equipped in the legs slot.
    /// </summary>
    private Item equippedLegsItem;

    /// <summary>
    /// The original item equipped in the head slot.
    /// </summary>
    [SerializeField]
    private Item originalEquippedHeadItem;

    /// <summary>
    /// The original item equipped in the body slot.
    /// </summary>
    [SerializeField]
    private Item originalEquippedBodyItem;

    /// <summary>
    /// The original item equipped in the legs slot.
    /// </summary>
    [SerializeField]
    private Item originalEquippedLegsItem;

    /// <summary>
    /// UI element representing the currently equipped head item.
    /// </summary>
    public Image actuallyHeadUI;

    /// <summary>
    /// UI element representing the currently equipped body item.
    /// </summary>
    public Image actuallyBodyUI;

    /// <summary>
    /// UI element representing the currently equipped legs item.
    /// </summary>
    public Image actuallyFootsUI;

    /// <summary>
    /// Sets the initial UI sprites to show the original equipped items.
    /// </summary>
    private void Start()
    {
        actuallyHeadUI.sprite = originalEquippedHeadItem.icon;
        actuallyBodyUI.sprite = originalEquippedBodyItem.icon;
        actuallyFootsUI.sprite = originalEquippedLegsItem.icon;
    }

    /// <summary>
    /// Equips the given item to the player object.
    /// </summary>
    /// <param name="item">The item to be equipped.</param>
    public void EquipItem(Item item)
    {
        if (item != null && playerObject != null)
        {
            UnequipItemsOfSameCategory(item);
            ApplyItemEffects(item);
            item.isEquipped = true;
        }
    }

    /// <summary>
    /// Unequips the given item from the player object.
    /// </summary>
    /// <param name="item">The item to be unequipped.</param>
    public void UnEquipItem(Item item)
    {
        if (item != null && playerObject != null)
        {
            ApplyItemEffects(item);
            item.isEquipped = false;
        }
    }

    /// <summary>
    /// Applies the effects of the given item to the player object.
    /// </summary>
    /// <param name="item">The item whose effects are to be applied.</param>
    void ApplyItemEffects(Item item)
    {
        switch (item.itemType)
        {
            case ItemType.Head:
                ApplyHeadEquipment(item);
                equippedHeadItem = item;
                break;
            case ItemType.Body:
                ApplyBodyEquipment(item);
                equippedBodyItem = item;
                break;
            case ItemType.Shoes:
                ApplyLegsEquipment(item);
                equippedLegsItem = item;
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Applies the effects of head equipment to the player object.
    /// </summary>
    /// <param name="item">The head item to be equipped.</param>
    void ApplyHeadEquipment(Item item)
    {
        headObject.GetComponent<SpriteRenderer>().sprite = item.Cloth[0];
        actuallyHeadUI.sprite = item.icon;
    }

    /// <summary>
    /// Applies the effects of body equipment to the player object.
    /// </summary>
    /// <param name="item">The body item to be equipped.</param>
    void ApplyBodyEquipment(Item item)
    {
        torse.GetComponent<SpriteRenderer>().sprite = item.Cloth[0];
        lLeageadObject.GetComponent<SpriteRenderer>().sprite = item.Cloth[1];
        RLeageadObject.GetComponent<SpriteRenderer>().sprite = item.Cloth[2];
        pelvis.GetComponent<SpriteRenderer>().sprite = item.Cloth[3];
        actuallyBodyUI.sprite = item.icon;
    }

    /// <summary>
    /// Applies the effects of leg equipment to the player object.
    /// </summary>
    /// <param name="item">The leg item to be equipped.</param>
    void ApplyLegsEquipment(Item item)
    {
        LShoesObject.GetComponent<SpriteRenderer>().sprite = item.Cloth[0];
        RShoesObject.GetComponent<SpriteRenderer>().sprite = item.Cloth[1];
        actuallyFootsUI.sprite = item.icon;
    }

    /// <summary>
    /// Removes the head equipment from the player object.
    /// </summary>
    void RemoveHeadEquipment()
    {
        headObject.GetComponent<SpriteRenderer>().sprite = originalEquippedHeadItem.Cloth[0];
    }

    /// <summary>
    /// Removes the body equipment from the player object.
    /// </summary>
    void RemoveBodyEquipment()
    {
        torse.GetComponent<SpriteRenderer>().sprite = originalEquippedBodyItem.Cloth[0];
        lLeageadObject.GetComponent<SpriteRenderer>().sprite = originalEquippedBodyItem.Cloth[1];
        RLeageadObject.GetComponent<SpriteRenderer>().sprite = originalEquippedBodyItem.Cloth[2];
        pelvis.GetComponent<SpriteRenderer>().sprite = originalEquippedBodyItem.Cloth[3];
    }

    /// <summary>
    /// Removes the leg equipment from the player object.
    /// </summary>
    void RemoveLegsEquipment()
    {
        LShoesObject.GetComponent<SpriteRenderer>().sprite = originalEquippedLegsItem.Cloth[0];
        RShoesObject.GetComponent<SpriteRenderer>().sprite = originalEquippedLegsItem.Cloth[1];
    }

    /// <summary>
    /// Unequips items of the same category if already equipped.
    /// </summary>
    /// <param name="newItem">The new item to be equipped.</param>
    public void UnequipItemsOfSameCategory(Item newItem)
    {
        switch (newItem.itemType)
        {
            case ItemType.Head:
                if (equippedHeadItem != null)
                {
                    RemoveHeadEquipment();
                    actuallyHeadUI.sprite = originalEquippedHeadItem.icon;
                    equippedHeadItem.isEquipped = false;
                }
                break;
            case ItemType.Body:
                if (equippedBodyItem != null)
                {
                    RemoveBodyEquipment();
                    actuallyBodyUI.sprite = originalEquippedBodyItem.icon;
                    equippedBodyItem.isEquipped = false;
                }
                break;
            case ItemType.Shoes:
                if (equippedLegsItem != null)
                {
                    RemoveLegsEquipment();
                    actuallyFootsUI.sprite = originalEquippedLegsItem.icon;
                    equippedLegsItem.isEquipped = false;
                }
                break;
            default:
                Debug.LogWarning("Unsupported item type: " + newItem.itemType);
                break;
        }
    }
}
