using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Represents an item in the inventory.
/// </summary>
[System.Serializable]
[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class Item : ScriptableObject
{
    /// <summary>
    /// The name of the item.
    /// </summary>
    public new string name = "New Item";

    /// <summary>
    /// The icon representing the item.
    /// </summary>
    public Sprite icon = null;

    /// <summary>
    /// Array of sprites representing different clothing parts.
    /// </summary>
    public Sprite[] Cloth = null;

    /// <summary>
    /// The price of the item.
    /// </summary>
    public int price = 0;

    /// <summary>
    /// The type of the item.
    /// </summary>
    public ItemType itemType;

    /// <summary>
    /// Flag indicating if the item is equipped.
    /// </summary>
    public bool isEquipped = false;

    /// <summary>
    /// Flag indicating if the item has been purchased.
    /// </summary>
    public bool Purchased = false;

    /// <summary>
    /// Specifies the type of item.
    /// </summary>
    public enum ItemType
    {
        Head,
        Body,
        Shoes,
    }

    /// <summary>
    /// Serializes the item to JSON.
    /// </summary>
    /// <returns>The JSON representation of the item.</returns>
    public string SerializeToJson()
    {
        return JsonUtility.ToJson(this);
    }

    /// <summary>
    /// Deserializes the item from JSON.
    /// </summary>
    /// <param name="json">The JSON string representing the item.</param>
    /// <returns>The deserialized item.</returns>
    public static Item DeserializeFromJson(string json)
    {
        return JsonUtility.FromJson<Item>(json);
    }
}
