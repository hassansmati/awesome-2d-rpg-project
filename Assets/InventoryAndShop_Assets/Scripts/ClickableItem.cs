using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script represents a clickable UI element associated with an item.
/// It handles click events and interacts with the InventoryUI script.
/// </summary>
public class ClickableItem : MonoBehaviour
{
    /// <summary>
    /// Reference to the item associated with this UI element.
    /// </summary>
    public Item item;

    /// <summary>
    /// Reference to the InventoryUI script.
    /// </summary>
    public InventoryUI inventoryUI;

    /// <summary>
    /// Reference to the GameObject indicating whether the item is equipped.
    /// </summary>
    public GameObject equipped;

    private Button button; // Cached Button component

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// It caches the Button component and the reference to the "equipped" GameObject.
    /// </summary>
    private void Awake()
    {
        button = GetComponent<Button>();
        //equipped = transform.Find("Check").gameObject;
    }

    /// <summary>
    /// Start is called before the first frame update.
    /// It adds a click listener to the button and sets the initial state of the "equipped" GameObject.
    /// </summary>
    private void Start()
    {
        button.onClick.AddListener(OnClick);
        SetEquippedState(item != null && item.isEquipped);
    }

    /// <summary>
    /// Sets the state of the "equipped" GameObject based on whether the item is equipped.
    /// </summary>
    private void SetEquippedState(bool isEquipped)
    {
        if (equipped != null)
        {
            equipped.SetActive(isEquipped);
        }
    }

    /// <summary>
    /// Handles the click event on the UI element.
    /// Invokes the OnItemClick() method on the InventoryUI script.
    /// </summary>
    private void OnClick()
    {
        inventoryUI.OnItemClick(item);
    }
}
