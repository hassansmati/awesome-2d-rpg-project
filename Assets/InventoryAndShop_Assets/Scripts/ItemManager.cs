using UnityEngine;

/// <summary>
/// This script manages the setup of items in the shop at runtime.
/// </summary>
public class ItemManager : MonoBehaviour
{
    /// <summary>
    /// Reference to the parent GameObject where items will be instantiated.
    /// </summary>
    public GameObject shopParent;

    /// <summary>
    /// Reference to the prefab representing an item in the shop.
    /// </summary>
    public GameObject itemPrefab;

    /// <summary>
    /// Reference to the player GameObject.
    /// </summary>
    public GameObject player;

    /// <summary>
    /// Reference to the InventoryUI script.
    /// </summary>
    public InventoryUI inventoryUI;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// It finds and sets a reference to the player GameObject.
    /// </summary>
    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    /// <summary>
    /// Start is called before the first frame update.
    /// It resets the isEquipped property for all items and sets up items in the shop.
    /// </summary>
    void Start()
    {
        ResetIsEquippedForAllItems();
        SetupItemsInShop();
    }

    /// <summary>
    /// Resets the isEquipped property for all items.
    /// </summary>
    private void ResetIsEquippedForAllItems()
    {
        // Find all Item assets in the project
        Item[] items = Resources.LoadAll<Item>("Inventory");

        // Loop through each item and reset its isEquipped property
        foreach (Item item in items)
        {
            item.isEquipped = false;
        }
    }

    /// <summary>
    /// Sets up items in the shop by instantiating their prefabs.
    /// </summary>
    private void SetupItemsInShop()
    {
        // Find all Item assets in the project
        Item[] items = Resources.LoadAll<Item>("Inventory");
        int i = 0;

        // Loop through each item and instantiate its prefab under the shop parent
        foreach (Item item in items)
        {
            // Instantiate the item prefab
            GameObject newItem = Instantiate(itemPrefab, shopParent.transform);

            // Set a name for the instantiated item
            newItem.name = "Item" + i;

            // Set references and properties on the instantiated prefab
            ClothingItem clothingItem = newItem.GetComponent<ClothingItem>();
            if (clothingItem != null)
            {
                clothingItem.inventory = player.GetComponent<Inventory>();
                clothingItem.inventoryUI = inventoryUI;
                clothingItem.item = item;
            }

            // Increment the counter for naming
            i++;
        }
    }
}
