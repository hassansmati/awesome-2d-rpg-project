using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Represents the player's inventory and manages the adding, removing, and buying of items.
/// </summary>
public class Inventory : MonoBehaviour
{
    /// <summary>
    /// Maximum number of items the inventory can hold.
    /// </summary>
    public int inventorySpace = 10;

    /// <summary>
    /// List to store the items in the inventory.
    /// </summary>
    public List<Item> items = new List<Item>();

    /// <summary>
    /// Reference to the EquippedItemsManager script.
    /// </summary>
    public EquippedItemsManager equippedItemsManager;

    /// <summary>
    /// Reference to the GameManager script.
    /// </summary>
    public GameManager gameManager;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// It finds and sets a reference to the GameManager script.
    /// </summary>
    private void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    /// <summary>
    /// Adds an item to the inventory if there is space available.
    /// Returns true if the item is added successfully, false otherwise.
    /// </summary>
    public bool AddItem(Item item)
    {
        if (items.Count < inventorySpace)
        {
            items.Add(item);

            // If the item is equipped, add it to the equipped items list
            if (item.isEquipped)
            {
                equippedItemsManager.AddEquippedItem(item);
            }

            return true;
        }
        else
        {
            Debug.Log("Inventory is full.");
            return false;
        }
    }

    /// <summary>
    /// Removes an item from the inventory.
    /// </summary>
    public void RemoveItem(Item item)
    {
        items.Remove(item);

        // If the item is equipped, remove it from the equipped items list
        if (item.isEquipped)
        {
            equippedItemsManager.RemoveEquippedItem(item);
        }
    }

    /// <summary>
    /// Buys an item and adds it to the inventory if the player has enough money.
    /// Returns true if the item is bought successfully, false otherwise.
    /// </summary>
    public bool BuyItem(Item item, int price)
    {
        // Check if the player has enough money to buy the item
        if (gameManager.Money >= price)
        {
            // Check if the item is already in the inventory
            if (items.Contains(item))
            {
                return false;
            }
            else
            {
                gameManager.Money -= price; // Deduct the price from the player's money
                AddItem(item); // Add the item to the inventory
                gameManager.UpdateCoinsUI();

                return true;
            }
        }
        else
        {
            Debug.Log("Not enough money to buy " + item.name + ".");
            return false;
        }
    }
}
