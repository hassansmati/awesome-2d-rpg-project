using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// This script manages the equipped items for the player.
/// </summary>
public class EquippedItemsManager : MonoBehaviour
{
    /// <summary>
    /// List to store the equipped items.
    /// </summary>
    public List<Item> equippedItems = new List<Item>();

    /// <summary>
    /// Adds an equipped item to the list if it's not already equipped.
    /// </summary>
    /// <param name="item">The item to add to the equipped items list.</param>
    public void AddEquippedItem(Item item)
    {
        if (!equippedItems.Contains(item))
        {
            equippedItems.Add(item);
            item.isEquipped = true;
        }
        else
        {
            Debug.Log(item.name + " is already equipped.");
        }
    }

    /// <summary>
    /// Removes an equipped item from the list.
    /// </summary>
    /// <param name="item">The item to remove from the equipped items list.</param>
    public void RemoveEquippedItem(Item item)
    {
        if (equippedItems.Contains(item))
        {
            equippedItems.Remove(item);
            item.isEquipped = false;
        }
        else
        {
            Debug.Log(item.name + " is not equipped.");
        }
    }

    // Optionally, you can add methods to get the list of equipped items or check if an item is equipped.
}
