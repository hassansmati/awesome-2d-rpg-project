using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Manages the behavior of a shop NPC.
/// </summary>
public class ShopMan : MonoBehaviour
{
    /// <summary>
    /// Reference to the popup canvas.
    /// </summary>
    public GameObject popupCanvas;

    /// <summary>
    /// Reference to the popup alert.
    /// </summary>
    public GameObject popupAlert;

    /// <summary>
    /// Name of the NPC.
    /// </summary>
    public string npcName = "ShopMan";

    /// <summary>
    /// Default popup message.
    /// </summary>
    public string popupDescription = "Hello strangers, are you here to buy something?";

    /// <summary>
    /// Text component for the popup message.
    /// </summary>
    public Text popupText;

    /// <summary>
    /// Text component for the NPC name.
    /// </summary>
    public Text npcNameText;

    private bool playerInRange; // Flag to indicate if the player is in range
    private GameObject player; // Reference to the player GameObject

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// It initializes references to the player GameObject.
    /// </summary>
    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    /// <summary>
    /// Start is called before the first frame update.
    /// It deactivates the popup canvas.
    /// </summary>
    void Start()
    {
        popupCanvas.SetActive(false);
    }

    /// <summary>
    /// Update is called once per frame.
    /// It checks if the player is in range and if the player interacts with the NPC to show the popup message.
    /// </summary>
    void Update()
    {
        if (playerInRange)
        {
            popupAlert.SetActive(true);
        }
        else
        {
            popupAlert.SetActive(false);
        }

        if (playerInRange && Input.GetKeyDown(KeyCode.E))
        {
            ShowPopup(popupDescription);
        }
    }

    /// <summary>
    /// OnTriggerEnter2D is called when the Collider2D other enters the trigger.
    /// It sets the playerInRange flag to true when the player enters the trigger zone.
    /// </summary>
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            playerInRange = true;
        }
    }

    /// <summary>
    /// OnTriggerExit2D is called when the Collider2D other has stopped touching the trigger.
    /// It sets the playerInRange flag to false when the player exits the trigger zone and hides the popup canvas.
    /// </summary>
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            playerInRange = false;
            popupCanvas.SetActive(false);
        }
    }

    /// <summary>
    /// Shows the popup message with the specified content.
    /// </summary>
    void ShowPopup(string message)
    {
        popupCanvas.SetActive(true);
        player.GetComponent<PlayerMovement>().DisablePlayerController();
    }
}
