using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Manages the user interface for the inventory system.
/// </summary>
public class InventoryUI : MonoBehaviour
{
    /// <summary>
    /// Reference to the Inventory script.
    /// </summary>
    public Inventory inventory;

    /// <summary>
    /// Prefab for the item slot.
    /// </summary>
    public GameObject itemSlotPrefab;

    /// <summary>
    /// Parent transform for item slots.
    /// </summary>
    public Transform itemsParent;

    /// <summary>
    /// Reference to the remove button.
    /// </summary>
    public GameObject RemoveButton;

    /// <summary>
    /// Reference to the equip button.
    /// </summary>
    public GameObject equipButton;

    /// <summary>
    /// Reference to the sell button.
    /// </summary>
    public GameObject sellButton;

    /// <summary>
    /// Currently selected item in the inventory.
    /// </summary>
    public Item selectedInventoryItem;

    /// <summary>
    /// Reference to the EquipmentManager script.
    /// </summary>
    public EquipmentManager equipmentManager;

    /// <summary>
    /// Reference to the GameManager script.
    /// </summary>
    public GameManager gameManager;

    /// <summary>
    /// Reference to the EquippedItemsManager script.
    /// </summary>
    public EquippedItemsManager equippedItemsManager;

    /// <summary>
    /// Initializes references to the GameManager.
    /// </summary>
    private void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    /// <summary>
    /// Updates the inventory UI and disables button interaction initially.
    /// </summary>
    void Start()
    {
        UpdateInventoryUI();
        SetButtonInteraction(false);
    }

    /// <summary>
    /// Checks if the player clicks outside of the equip and sell buttons to reset selected item and disable button interaction.
    /// </summary>
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!IsPointerOverButton(equipButton) && !IsPointerOverButton(sellButton) && !IsPointerOverButton(RemoveButton))
            {
                selectedInventoryItem = null;
                SetButtonInteraction(false);
            }
        }
    }

    /// <summary>
    /// Updates the inventory UI by clearing existing item slots and populating them with items from the inventory.
    /// </summary>
    public void UpdateInventoryUI()
    {
        foreach (Transform child in itemsParent)
        {
            Destroy(child.gameObject);
        }

        foreach (Item item in inventory.items)
        {
            GameObject newItemSlot = Instantiate(itemSlotPrefab, itemsParent);
            newItemSlot.GetComponent<ClickableItem>().inventoryUI = this.gameObject.GetComponent<InventoryUI>();
            newItemSlot.GetComponent<ClickableItem>().enabled = true;
            newItemSlot.GetComponent<ClickableItem>().item = item;

            Image itemIcon = newItemSlot.transform.GetChild(1).GetComponent<Image>();
            if (itemIcon != null && item.icon != null)
            {
                itemIcon.sprite = item.icon;
            }
        }
    }

    /// <summary>
    /// Handles the behavior when an item in the inventory is clicked.
    /// </summary>
    public void OnItemClick(Item item)
    {
        selectedInventoryItem = item;

        if (selectedInventoryItem.isEquipped)
        {
            RemoveButton.GetComponent<Button>().interactable = true;
            equipButton.GetComponent<Button>().interactable = false;
            sellButton.GetComponent<Button>().interactable = true;
        }
        else
        {
            RemoveButton.GetComponent<Button>().interactable = false;
            SetButtonInteraction(selectedInventoryItem != null);
        }
    }

    /// <summary>
    /// Sets the button interaction based on the specified state.
    /// </summary>
    void SetButtonInteraction(bool interactable)
    {
        equipButton.GetComponent<Button>().interactable = interactable;
        sellButton.GetComponent<Button>().interactable = interactable;
    }

    /// <summary>
    /// Equips the selected item and updates the inventory UI.
    /// </summary>
    public void EquipSelectedItem()
    {
        if (selectedInventoryItem != null)
        {
            equipmentManager.EquipItem(selectedInventoryItem);
            equippedItemsManager.AddEquippedItem(selectedInventoryItem);
            UpdateEquippedStatus(selectedInventoryItem);
            UpdateInventoryUI();
        }
        selectedInventoryItem = null;
        SetButtonInteraction(false);
    }

    /// <summary>
    /// Removes the selected item from the inventory and updates the UI.
    /// </summary>
    public void RemoveSelectedItem()
    {
        if (selectedInventoryItem != null)
        {
            UpdateInventoryUI();
            equippedItemsManager.RemoveEquippedItem(selectedInventoryItem);
            equipmentManager.UnEquipItem(selectedInventoryItem);
            equipmentManager.UnequipItemsOfSameCategory(selectedInventoryItem);
            UpdateInventoryUI();
        }
        selectedInventoryItem = null;
        SetButtonInteraction(false);
        RemoveButton.GetComponent<Button>().interactable = false;

    }

    /// <summary>
    /// Updates the equipped status of the specified item in the inventory UI.
    /// </summary>
    void UpdateEquippedStatus(Item item)
    {
        foreach (Transform slot in itemsParent)
        {
            ClickableItem clickableItem = slot.GetComponent<ClickableItem>();
            if (clickableItem != null && clickableItem.item == item)
            {
                clickableItem.equipped.SetActive(item.isEquipped);
                return;
            }
        }
    }

    /// <summary>
    /// Sells the selected item, removes it from the inventory, and updates the UI.
    /// </summary>
    public void SellSelectedItem()
    {
        if (selectedInventoryItem != null)
        {
            inventory.RemoveItem(selectedInventoryItem);
            UpdateInventoryUI();
            equippedItemsManager.RemoveEquippedItem(selectedInventoryItem);
            equipmentManager.UnEquipItem(selectedInventoryItem);
            gameManager.Money += selectedInventoryItem.price / 2;
            equipmentManager.UnequipItemsOfSameCategory(selectedInventoryItem);
            gameManager.UpdateCoinsUI();
            UpdateInventoryUI();
        }
        selectedInventoryItem = null;
        SetButtonInteraction(false);
        RemoveButton.GetComponent<Button>().interactable = false;

    }

    /// <summary>
    /// Checks if the mouse pointer is over a specific button.
    /// </summary>
    bool IsPointerOverButton(GameObject button)
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

        foreach (RaycastResult result in results)
        {
            if (result.gameObject == button)
            {
                return true;
            }
        }
        return false;
    }
}
